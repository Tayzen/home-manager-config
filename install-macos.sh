#!/bin/sh

# installing homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# installing me homebrew packages (unavailable on nix or in too old versions)
brew install nvim ollama maccy orbstack lm-studio vlc koekeishiya/formulae/yabai koekeishiya/formulae/skhd joshmedeski/sesh/sesh
brew install --cask raycast logseq readline xz zlib tcl-tk # The last 3 are dependencies from pyenv
brew services start ollama
skhd --start-service
yabai --start-service
