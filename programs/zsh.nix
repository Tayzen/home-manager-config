{ config, pkgs, ... }:
{
    programs.zsh = {
        enable = true;
        shellAliases = {
            ll = "ls -al";
            ping = "prettyping";
            ls = "eza --icons --color=auto";
            tree = "eza --icons --color=auto --tree";
            cat = "bat";
            grep = "rg";
            cd = "z";
            zz = "z -";
            nano = "micro";
            lf = "lfrun";
        };
        enableCompletion = false;
        autosuggestion.enable = false;
        syntaxHighlighting.enable = false;
        zplug = {
            enable = true;
            plugins = [
            { name = "marlonrichert/zsh-autocomplete"; }
            { name = "plugins/git"; tags = [from:oh-my-zsh]; }
            { name = "plugins/thefuck"; tags = [from:oh-my-zsh]; }
            { name = "plugins/zsh-autosuggestions"; tags = [from:oh-my-zsh]; }
            { name = "plugins/fast-syntax-highlighting"; tags = [from:oh-my-zsh]; }
            ];
        };
        initExtra = ''
            eval "$(starship init zsh)"
            eval "$(atuin init zsh)"
            eval "$(zoxide init zsh)"
            export PYENV_ROOT="$HOME/.pyenv"
            [[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
            eval "$(pyenv init -)"
            export PATH="$HOME/.deno/bin:$PATH"
        '';
    };
}
