{ pkgs, ... }:
let
  tmux-nerd-font-window-name = pkgs.tmuxPlugins.mkTmuxPlugin
    {
      pluginName = "tmux-nerd-font-window-name";
      version = "unstable-2024-02-03";
      rtpFilePath = "tmux-nerd-font-window-name.tmux";
      src = pkgs.fetchFromGitHub {
        owner = "joshmedeski";
        repo = "tmux-nerd-font-window-name";
        rev = "1c2cf6561677c0ab382388f472b4a1f893264d80";
        sha256 = "hA4dpD5nppIUYEdX/UpQrJt0Ch5JsjY+4dAwrrqgur4=";
      };
    };
in
{
  programs.tmux = {
    enable = true;
    historyLimit = 100000;
    plugins = with pkgs;
      [
        tmuxPlugins.fuzzback
        tmuxPlugins.sensible # TODO: remove?
        tmuxPlugins.better-mouse-mode
        tmuxPlugins.vim-tmux-navigator
        tmuxPlugins.battery
        tmuxPlugins.yank
        tmux-nerd-font-window-name
        {
          plugin = tmuxPlugins.resurrect;
          extraConfig = ''
            set -g @resurrect-strategy-vim 'session'
            set -g @resurrect-strategy-nvim 'session'
            set -g @resurrect-capture-pane-contents 'on'
          '';
        }
        {
          plugin = tmuxPlugins.continuum;
          extraConfig = ''
            set -g @continuum-restore 'on'
            set -g @continuum-boot 'on'
            set -g @continuum-save-interval '10'
          '';
        }
      ];
    extraConfig = ''
    ################################# Options ######################################

    # Better colors
    set -g default-terminal "$TERM"
    set-option -g default-terminal "$TERM"
    set-option -sa terminal-features ',$TERM:RGB'
    # set-option -g default-terminal "screen-256color"
    set-option -ga terminal-features ",$TERM:usstyle"
    set -sg terminal-overrides ",$TERM:RGB"
    set -as terminal-overrides ',*:Smulx=\E[4::%p1%dm'  # undercurl support
    set -as terminal-overrides ',*:Setulc=\E[58::2::%p1%{65536}%/%d::%p1%{256}%/%{255}%&%d::%p1%{255}%&%d%;m'  # underscore colours - needs tmux-3.0

    set -g base-index 1          # start indexing windows at 1 instead of 0
    set -g pane-base-index 1     # same for panes
    set-window-option -g pane-base-index 1
    set -g detach-on-destroy off # don't exit from tmux when closing a session
    set -g escape-time 0         # zero-out escape time delay
    set -g history-limit 1000000 # increase history size (from 2,000)
    set -g mouse on              # enable mouse support
    set -g renumber-windows on   # renumber all windows when any window is closed
    set -g set-clipboard on      # use system clipboard
    set -g status-interval 1     # update the status bar every 3 seconds

    ############################ Status bar theming ################################

    set -g status-position top       # macOS / darwin style
    set -g status-style "bg=default" # transparent
    set -g status-left "#[fg=blue,bold]#S "
    set -ga status-left ' #[fg=white,nobold]#(gitmux -cfg $HOME/.config/tmux/gitmux.yml "#{pane_current_path}") -  '
    set -g status-left-length 200    # increase length (from 10)
    set -g status-right ""
    set -g window-status-current-format ' #[fg=magenta]#W'
    set -g window-status-format ' #[fg=gray]#W'
    set -Fg 'status-format[1]' '#{status-format[0]}'
    set -g 'status-format[1]' ""
    set -g status 2

    ############################### Color theming ##################################

    set -g message-command-style bg=default,fg=yellow
    set -g message-style bg=default,fg=yellow
    set -g mode-style bg=default,fg=yellow
    set -g pane-active-border-style 'fg=magenta,bg=default'
    set -g pane-border-style 'fg=brightblack,bg=default'

    ################################ Keybindings ###################################

    # Change prefix to ctrl+space (default: ctrl+b)
    unbind C-b
    set -g prefix C-Space
    bind C-Space send-prefix

    # Visual mode
    bind-key -T copy-mode-vi v send-keys -X begin-selection
    bind-key -T copy-mode-vi C-v send-keys -X rectangle-toggle
    bind-key -T copy-mode-vi y send-keys -X copy-selection-and-cancel

    # Search in buffer
    bind-key / copy-mode \; send-key ?

    # Split in cwd
    bind v split-window -v -c "#{pane_current_path}"
    bind h split-window -h -c "#{pane_current_path}"

    # New window in cwd
    bind c new-window -c "#{pane_current_path}"

    # Synchronized panes
    bind-key g set-window-option synchronize-panes\; display-message "synchronize-panes is now #{?pane_synchronized,on,off}"

    # Sesh menu
    bind-key "T" run-shell "sesh connect \"$(
      sesh list -tz | fzf-tmux -p 55%,60% \
        --no-sort --border-label ' sesh ' --prompt '⚡  ' \
        --header '  ^a all ^t tmux ^x zoxide ^d tmux kill ^f find' \
        --bind 'tab:down,btab:up' \
        --bind 'ctrl-a:change-prompt(⚡  )+reload(sesh list)' \
        --bind 'ctrl-t:change-prompt(🪟  )+reload(sesh list -t)' \
        --bind 'ctrl-x:change-prompt(📁  )+reload(sesh list -z)' \
        --bind 'ctrl-f:change-prompt(🔎  )+reload(fd -H -d 2 -t d -E .Trash . ~)' \
        --bind 'ctrl-d:execute(tmux kill-session -t {})+change-prompt(⚡  )+reload(sesh list)'
    )\""

    set-option -g default-shell "/bin/zsh"
    set-option -g default-command "/bin/zsh"
    '';
  };
}
