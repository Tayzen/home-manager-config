{ config, pkgs, ... }:
{
    programs.micro = {
        enable = true;
        settings = {
            # Settings
            autoindent = true;
            autosu = true;
            backup = true;
            clipboard = "external";
            colorscheme = "simple";
            cusorline = true;
            diffgutter = true;
            fileformat = "unix";
            hlsearch = true;
            indentchar = " ";
            infobar = true;
            matchbrace = true;
            mkparents = true;
            mouse = true;
            multiopen = "tab";
            reload = "auto";
            rmtrailingws = true;
            ruler = true;
            savecursor = true;
            saveundo = true;
            scrollbar = true;
            statusline = true;
            tabsize = 2;
            tabstospaces = true;
            xterm = true; # could create errors

            # Plugins
            autoclose = true;
            comment = true;
            ftoptions = true;
            linter = true;
            literate = true;
            status = true;
            diff = true;
            wc = true;
            fzf = true;
            editorconfig = true;
            misspell = true;
            quoter = true;
            jump = true;
            detectindent = true;
            # lsp = true;

            # Language-specific
            "$.py" = {
                tabsize = 4;
            };
        };
    };
}
