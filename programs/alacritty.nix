{ config, pkgs, ... }:
{
  programs.alacritty = {
    enable = true;
    settings = {
      env = {
        TERM = "alacritty";
      };
      window = {
        opacity = 0.9;
        dynamic_title = true;
        padding = {
          x = 14;
          y = 12;
        };
        dynamic_padding = false;
        decorations = "buttonless";
      };
      scrolling = {
        history = 100000;
        multiplier = 3;
      };

      font = {
        size = 14;
        normal = {
          family = "CaskaydiaCove Nerd Font Mono";
          style = "SemiLight";
        };
        bold = {
          family = "CaskaydiaCove Nerd Font Mono";
          style = "Bold";
        };
        italic = {
          family = "CaskaydiaCove Nerd Font Mono";
          style = "Italic";
        };
        bold_italic = {
          family = "CaskaydiaCove Nerd Font Mono";
          style = "Bold Italic";
        };
      };

      colors = {
          primary = {
            background = "0x292d3e";
            foreground = "0xe8e8e8";
          };
          cursor = {
            text = "0x292d3e";
            cursor = "0x959dcb";
          };
          normal = {
            black = "0x292d3e";
            red = "0xf07178";
            green = "0xc3e88d";
            yellow = "0xffcb6b";
            blue = "0x82aaff";
            magenta = "0xc792ea";
            cyan = "0x89ddff";
            white = "0x959dcb";
          };
          bright = {
            black = "0x676e95";
            red = "0xf07178";
            green = "0xc3e88d";
            yellow = "0xffcb6b";
            blue = "0x82aaff";
            magenta = "0xc792ea";
            cyan = "0x89ddff";
            white = "0xffffff";
          };
          indexed_colors = [
            { index = 16; color = "0xf78c6c"; }
            { index = 17; color = "0xff5370"; }
            { index = 18; color = "0x444267"; }
            { index = 19; color = "0x32374d"; }
            { index = 20; color = "0x8796b0"; }
            { index = 21; color = "0x959dcb"; }
          ];
      };

      bell = {
        animation = "EaseOut";
        duration = 200;
        color = "#292d3e";
      };

      selection = {
        save_to_clipboard = false;
      };

      cursor = {
        style = {
          shape = "Block";
        };
        unfocused_hollow = true;
      };

      # general.live_config_reload = true;

      keyboard = { # TODO: to document
        bindings = [
          {
            key = "T";
            mods = "Command";
            chars = "\\u0000c";
          }
          {
            key = "W";
            mods = "Command";
            chars = "\\u0000x";
          }
          {
            key = "Tab";
            mods = "Control";
            chars = "\\u0000n";
          }
          {
            key = "Tab";
            mods = "Shift|Control";
            chars = "\\u0000p";
          }
          {
            key = "L";
            mods = "Command";
            chars = "clear\n";
          }
          {
            key = "F";
            mods = "Command";
            chars = "\\u0000?";
          }
          {
            key = "D";
            mods = "Command";
            chars = "\\u0000d";
          }
          { # TODO: open in a floating term?
            key = "G";
            mods = "Command";
            chars = "lazygit\n";
          }
          # TODO: open quite-intriguing in floating term
          {
            key = "&";
            mods = "Command";
            chars = "\\u00001";
          }
          {
            key = "é";
            mods = "Command";
            chars = "\\u00002";
          }
          {
            key = "\"";
            mods = "Command";
            chars = "\\u00003";
          }
          {
            key = "'";
            mods = "Command";
            chars = "\\u00004";
          }
          {
            key = "(";
            mods = "Command";
            chars = "\\u00005";
          }
          {
            key = "§";
            mods = "Command";
            chars = "\\u00006";
          }
          {
            key = "è";
            mods = "Command";
            chars = "\\u00007";
          }
          {
            key = "!";
            mods = "Command";
            chars = "\\u00008";
          }
          {
            key = "ç";
            mods = "Command";
            chars = "\\u00009";
          }
        ];
      };

      # terminal = {
      #   shell = {
      #     program = "zsh";
      #     args = [
      #       "--login"
      #     ];
      #   };
      # };
    };
  };
}
