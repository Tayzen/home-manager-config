{ config, pkgs, ... }:
{
    programs.vscode = {
        enable = true;
        extensions = with pkgs.vscode-extensions; [
            # Remote access
            ms-vscode-remote.remote-containers
            ms-vscode-remote.remote-ssh

            # Themes
            pkief.material-icon-theme

            # Spellchecker
            davidlday.languagetool-linter

            # Code completion
            christian-kohler.path-intellisense

            # Data exploration
            grapecity.gc-excelviewer
            mechatroner.rainbow-csv

            # Utilities
            alefragnani.bookmarks
            alefragnani.project-manager
            adpyke.codesnap
            editorconfig.editorconfig
            eamodio.gitlens
            gitlab.gitlab-workflow
            gruntfuggly.todo-tree
            kamikillerto.vscode-colorize
            ms-vscode.hexeditor
            tomoki1207.pdf
            esbenp.prettier-vscode
            codezombiech.gitignore

            # Markdown
            yzhang.markdown-all-in-one
            davidanson.vscode-markdownlint
            shd101wyy.markdown-preview-enhanced

            # Python
            ms-python.isort
            ms-python.python
            ms-python.vscode-pylance
            njpwerner.autodocstring

            # Other languages
            redhat.java
            redhat.vscode-xml
            redhat.vscode-yaml
            mikestead.dotenv
            bbenoist.nix

            # Virtualization
            ms-azuretools.vscode-docker
            ms-kubernetes-tools.vscode-kubernetes-tools
        ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
            # Remote access
            {
                name = "remote-ssh-edit";
                publisher = "ms-vscode-remote";
                version = "0.47.2";
                sha256 = "1hp6gjh4xp2m1xlm1jsdzxw9d8frkiidhph6nvl24d0h8z34w49g";
            }

            # Themes
            {
                name = "material-palenight-theme";
                publisher = "whizkydee";
                version = "2.0.3";
                sha256 = "ab3da9cfd2659ced8e5777899d1ab36dc89cd65ce9974562ca0c2136364e5a92";
            }
            {
                name = "shades-of-purple";
                publisher = "ahmadawais";
                version = "7.2.0";
                sha256 = "07e774232266c8b58b7d274a539fb9c02e5faf643d334a5b483fa95015d6c255";
            }

            # Code completion
            {
                name = "vscodeintellicode";
                publisher = "VisualStudioExptTeam";
                version = "1.2.30";
                sha256 = "7f61a7f96d101cdf230f96821be3fddd8f890ebfefb3695d18beee43004ae251";
            }
            {
                name = "codeium";
                publisher = "Codeium";
                version = "1.7.38";
                sha256 = "6d3fbd9e5863d2dad7d657c2758b1bb2b17648238dc8f682edca89b66d77018c";
            }

            # Data exploration
            {
                name = "vscode-sqlite";
                publisher = "alexcvzz";
                version = "0.14.1";
                sha256 = "8ce42446006453026ea43f9c468fca45a84546cf365f72b8f43c92c3a1a553c5";
            }

            # Utilities
            {
                name = "wordcount";
                publisher = "ms-vscode";
                version = "0.1.0";
                sha256 = "41be0a5372b436c53b53619666c700e96024e34e916e700ea4822fbc82389a98";
            }
        ];
    };
}
