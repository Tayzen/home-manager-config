#!/bin/bash

# Check the current status of Tailscale
if tailscale status > /dev/null 2>&1; then
    echo "Tailscale is currently UP. Toggling DOWN and commenting IP lines."

    # Comment lines starting with '100.'
    sudo sed -i '' "/^100\./s/^/#/" /etc/hosts # the parameter '' is only needed on MacOS

    sudo tailscale down
else
    echo "Tailscale is currently DOWN. Toggling UP and uncommenting IP lines."

    # Uncomment lines starting with '#100.'
    sudo sed -i '' "/^#100\./s/^#//" /etc/hosts # the parameter '' is only needed on MacOS

    sudo tailscale up
fi

# Flush DNS cache to apply the changes
sudo dscacheutil -flushcache
sudo killall -HUP mDNSResponder
