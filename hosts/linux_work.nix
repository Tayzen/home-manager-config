{ config, pkgs, ... }:
{
  nixpkgs.config.allowUnfree = true;

  home.packages = [
    # Terminal apps
    # pkgs.nodejs_18
    pkgs.nodenv
    pkgs.newman

    # Graphical apps
    pkgs.alacritty
    pkgs.vscode # Unfree
    pkgs.jetbrains.idea-community-src # intellij
    pkgs.scala_2_11
    # pkgs.sbt # Doesn't work on my project
    pkgs.postman
  ];

  fonts.fontconfig.enable = true;

  imports = [
    ./common.nix
    ../programs/alacritty.nix
    ../programs/vscode.nix
  ];

  programs.zsh = {
    shellAliases = {
      squid_work = "sudo -- sh -c 'rm -f /etc/squid/squid.conf; cp /etc/squid/squid_work.conf /etc/squid/squid.conf; systemctl restart squid'";
      squid_direct = "sudo -- sh -c 'rm -f /etc/squid/squid.conf; cp /etc/squid/squid_direct.conf /etc/squid/squid.conf; systemctl restart squid'";
    };

    initExtra = ''
    eval "$(starship init zsh)"
    eval "$(atuin init zsh)"
    eval "$(zoxide init zsh)"

    # Command will probably crash if sdkman isn't installed
    # TODO: script to auto-install sdkman
    export SDKMAN_DIR="$HOME/.sdkman"
    [[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

    # Command will probably crash if nvm isn't installed
    # TODO: script to auto-install nvm
    export NVM_DIR="$([ -z "$XDG_CONFIG_HOME" ] && printf %s "$HOME/.nvm" || printf %s "$XDG_CONFIG_HOME/nvm")"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

    nvm use 18.18.2
    '';
  };

  home.sessionVariables = {
    http_proxy = "http://squid.proxy:3128";
    https_proxy = "http://squid.proxy:3128";
    HTTP_PROXY = "http://squid.proxy:3128";
    HTTPS_PROXY = "http://squid.proxy:3128";
    no_proxy = "localhost,127.0.0.1";
    NO_PROXY = "localhost,127.0.0.1";
  };
}
