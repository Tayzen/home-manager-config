{ config, pkgs, ... }:

# TODO: shell script to select an host file and generate the corresponding home.nix
{
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  # TODO: username variable
  home.username = "benjamin";
  home.homeDirectory = (if pkgs.stdenv.isDarwin then
    "/Users/benjamin" # MacOS
  else
    "/home/benjamin" # Linux
  );

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.11"; # Please read the comment before changing.
  # TODO: unstable pkgs?

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = [
    # Terminal apps
    pkgs.coreutils
    pkgs.zsh
    pkgs.nushell
    pkgs.htop
    pkgs.btop
    pkgs.wget
    pkgs.curl
    pkgs.neofetch
    pkgs.nerdfonts
    pkgs.git
    pkgs.lazygit
    pkgs.lazydocker
    pkgs.navi
    pkgs.tldr
    pkgs.cheat
    pkgs.eza
    pkgs.prettyping
    pkgs.fzf
    pkgs.tmux
    pkgs.gitmux
    pkgs.tree
    pkgs.bat
    pkgs.fd
    pkgs.ripgrep
    pkgs.du-dust
    pkgs.atuin
    pkgs.zoxide
    pkgs.tailspin
    pkgs.thefuck
    pkgs.watch
    pkgs.entr
    pkgs.scc
    pkgs.jq
    pkgs.yq-go
    pkgs.lf
    # pkgs.neovim # Neovim isn't available in a nightly version
    pkgs.micro
    pkgs.cargo
    pkgs.pipx
    pkgs.pyenv
    pkgs.poetry
    pkgs.k9s
    pkgs.texliveFull
    pkgs.pandoc
    pkgs.plantuml
    pkgs.tcpdump
    pkgs.nmap
    pkgs.starship
    pkgs.httpie
    pkgs.chafa
    pkgs.glow
    pkgs.ffmpeg
    pkgs.ffmpegthumbnailer
    pkgs.imagemagick
    pkgs.poppler_utils
    pkgs.unzip
    pkgs.p7zip
    pkgs.unrar
    pkgs.catdoc
    pkgs.python311Packages.docx2txt
    pkgs.odt2txt
    pkgs.exiftool
    pkgs.libcdio
    # pkgs.mcomix # issue to install a dependency, try to add it later
    pkgs.mutagen

    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # Shell scripts
    (pkgs.writeShellScriptBin "tm" ''
      tmux attach -t default || tmux new -s default
    '')
    (pkgs.writeShellScriptBin "lfrun" ''
      #!/bin/sh
      set -e

      if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
        lf "$@"
      else
        [ ! -d "$HOME/.cache/lf" ] && mkdir --parents "$HOME/.cache/lf"
        lf "$@" 3>&-
      fi
    '')
    (pkgs.writeShellScriptBin "preview" (builtins.readFile ../scripts/preview.sh))
    (pkgs.writeShellScriptBin "tailscale_toggle" (builtins.readFile ../scripts/tailscale_toggle.sh))
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';

    "${config.xdg.configHome}" = {
      source = ../dotfiles;
      recursive = true;
    };

  };

  imports = [
    ../programs/git.nix
    ../programs/micro.nix
    ../programs/tmux.nix
    ../programs/zsh.nix
  ];


  # Home Manager can also manage your environment variables through
  # 'home.sessionVariables'. If you don't want to manage your shell through Home
  # Manager then you have to manually source 'hm-session-vars.sh' located at
  # either
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  ~/.local/state/nix/profiles/profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/benjamin/etc/profile.d/hm-session-vars.sh
  #
  home.sessionVariables = {
    EDITOR = "nvim";
    LF_ICONS = ''\
*.7z=:\
*.aac=:\
*.ace=:\
*.alz=:\
*.arc=:\
*.arj=:\
*.asf=:\
*.atom=:\
*.au=:\
*.avi=:\
*.bash=:\
*.bash_history=:\
*.bashprofile=:\
*.bashrc=:\
*.bmp=:\
*.bz2=:\
*.bz=:\
*.c=:\
*.cab=:\
*.cc=:\
*.cfg=:\
*.cgm=:\
*.clang-format=:\
*.clj=:\
*.cmd=:\
*.coffee=:\
*.cpio=:\
*.cpp=:\
*.css=:\
*.d=:\
*.dart=:\
*.deb=:\
*.dl=:\
*.DS_Store=:\
*.dwm=:\
*.dz=:\
*.ear=:\
*.emf=:\
*.env=:\
*.erl=:\
*.esd=:\
*.exs=:\
*.fish=:\
*.flac=:\
*.flc=:\
*.fli=:\
*.flv=:\
*.fs=:\
*.gif=:\
*.git=:\
*.gitattributes=:\
*.gitconfig=:\
*.github=:\
*.gitignore=:\
*.gitignore_global=:\
*.gitkeep=:\
*.gitmodules=:\
*.gl=:\
*.go=:\
*.gz=:\
*.h=:\
*.hh=:\
*.hidden=:\
*.hpp=:\
*.hs=:\
*.html=:\
*.hyper.js=:\
*.jar=:\
*.java=:\
*.jl=:\
*.jpeg=:\
*.jpg=:\
*.js=:\
*.json=:\
*.jsx=:\
*.lha=:\
*.lrz=:\
*.lua=:\
*.lz4=:\
*.lz=:\
*.lzh=:\
*.lzma=:\
*.lzo=:\
*.m2v=:\
*.m4a=:\
*.m4v=:\
*.map=:\
*.md=:\
*.mdx=:\
*.mid=:\
*.midi=:\
*.mjpeg=:\
*.mjpg=:\
*.mka=:\
*.mkv=:\
*.mng=:\
*.mov=:\
*.mp3=:\
*.mp4=:\
*.mp4v=:\
*.mpc=:\
*.mpeg=:\
*.mpg=:\
*.nix=:\
*.npmignore=:\
*.npmrc=:\
*.nuv=:\
*.nvmrc=:\
*.oga=:\
*.ogg=:\
*.ogm=:\
*.ogv=:\
*.ogx=:\
*.opus=:\
*.pbm=:\
*.pcx=:\
*.pdf=:\
*.pgm=:\
*.php=:\
*.pl=:\
*.png=:\
*.ppm=:\
*.pro=:\
*.ps1=:\
*.py=:\
*.qt=:\
*.ra=:\
*.rar=:\
*.rb=:\
*.rm=:\
*.rmvb=:\
*.rpm=:\
*.rs=:\
*.rvm=:\
*.rz=:\
*.sar=:\
*.scala=:\
*.sh=:\
*.skhdrc=:\
*.sol=ﲹ:\
*.spx=:\
*.svg=:\
*.svgz=:\
*.swm=:\
*.t7z=:\
*.tar=:\
*.taz=:\
*.tbz2=:\
*.tbz=:\
*.tga=:\
*.tgz=:\
*.tif=:\
*.tiff=:\
*.tlz=:\
*.tmux.conf=:\
*.trash=:\
*.ts=:\
*.tsx=:\
*.txz=:\
*.tz=:\
*.tzo=:\
*.tzst=:\
*.vim=:\
*.vimrc=:\
*.vob=:\
*.vscode=:\
*.war=:\
*.wav=:\
*.webm=:\
*.wim=:\
*.xbm=:\
*.xcf=:\
*.xpm=:\
*.xspf=:\
*.xwd=:\
*.xz=:\
*.yabairc=:\
*.yaml=פּ:\
*.yarn-integrity=:\
*.yarnrc=:\
*.yml=פּ:\
*.yuv=:\
*.z=:\
*.zip=:\
*.zoo=:\
*.zprofile=:\
*.zprofile=:\
*.zsh=:\
*.zsh_history=:\
*.zshrc=:\
*.zst=:\
*bin=:\
*config=:\
*docker-compose.yml=:\
*dockerfile=:\
*gradle=:\
*gruntfile.coffee=:\
*gruntfile.js=:\
*gruntfile.ls=:\
*gulpfile.coffee=:\
*gulpfile.js=:\
*gulpfile.ls=:\
*include=:\
*lib=:\
*localized=:\
*node_modules=:\
*package.json=:\
*rubydoc=:\
*tsconfig.json=:\
*yarn.lock=:\
di=:\
dt=:\
ex=:\
fi=:\
ln=:\
or=:\
ow=:\
st=:\
tw=:\
'';
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
