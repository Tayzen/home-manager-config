{ config, pkgs, ... }:

# TODO: brew integration
{
  home.packages = [
    # Terminal apps
    pkgs.nodejs_23  # TODO: permission issues, impossible to install packages
    pkgs.zulu # JDK compatible with ARM Macs (not tested)

    # Graphical apps
    # pkgs.iterm2 # MacOS only
    pkgs.utm # MacOS only
  ];

  imports = [
    ./common_graphical.nix
  ];
}
