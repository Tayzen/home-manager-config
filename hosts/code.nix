{ config, pkgs, ... }:
{
  home.packages = [
    # Terminal apps
    pkgs.nodejs_23
  ];

  imports = [
    ./common.nix
  ];
}
