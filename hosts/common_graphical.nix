{ config, pkgs, ... }:
{
  # Enable unfree packages (necesary for vscode, spotify and discord)
  nixpkgs.config.allowUnfree = true;

  home.packages = [
    # Terminal apps
    pkgs.yt-dlp

    # Graphical apps
    pkgs.alacritty
    pkgs.wireshark
    pkgs.syncthing
    pkgs.vscode # Unfree
    pkgs.discord # Unfree
    pkgs.spotify # Unfree
  ];

  imports = [
    ./common.nix
    ../programs/alacritty.nix
    ../programs/vscode.nix
  ];
}
