#!/bin/sh

nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
nix-channel --update
nix-shell '<home-manager>' -A install
git clone https://gitlab.com/Tayzen/neovim-config.git dotfiles/nvim
home-manager switch
