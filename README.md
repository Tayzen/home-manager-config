# Home manager config

![Screenshot](imgs/screenshot.png)

A project to manage my dotfiles and install my programs using `home-manager`.

## Requirements

It works both on Linux and macOS.

The requirements are very minimal; you only need `bash` and `git` installed on your machine.

## Usage

1. Clone this repo in your `~/.config/home-manager` folder: `git clone https://gitlab.com/Tayzen/home-manager-config.git ~/.config/home-manager`.
2. Open the `home.nix` file and edit `home.homeDirectory`, `home.username` and the list of programs to install: `home.packages` (some are unfree, some are exclusive to Linux, other exclusive to macOS so you can't use them all at once).
3. Run the `install.sh` script.

Additional information: The installation script installs nix and home-manager, clones my nvim config (which has its own dedicated repo), and runs the command `home-manager switch`. So, if you don't care about my nvim config (you'll be wrong), you can manually install the tools and run the command.

## Special thanks

* The teams behind nix and home-manager
* Hugo Reeves for inspiring me to use home-manager (even if my config isn't as evolved and well-thought-out as his)
* [Dreams of code](https://www.youtube.com/@dreamsofcode) for make me want to use tmux and neovim
* Phil294 for the micro keybindings ([the repo here](https://github.com/phil294/VSCode-keybindings-for-micro-editor-and-tty))
* [haseebmajid](https://haseebmajid.dev/posts/2023-07-10-setting-up-tmux-with-nix-home-manager/) for the example of tmux configuration using nix
* Josh Medeski for tmux, alacritty configurations
* Brodie Robertson for some part of my lf config
* [This repo by horriblename](https://github.com/horriblename/lfimg-sixel/tree/master) for my preview script
