#!/bin/bash

# Nix installation
if [ "$(uname -s)" == "Darwin" ]; then
  # macOS
  sh <(curl -L https://nixos.org/nix/install)
elif [ "$(uname -s)" == "Linux" ]; then
  # GNU/Linux
  sh <(curl -L https://nixos.org/nix/install) --daemon
else
  echo "The OS is not compatible, it should be MacOS or GNU/Linux"
  exit 1
fi
